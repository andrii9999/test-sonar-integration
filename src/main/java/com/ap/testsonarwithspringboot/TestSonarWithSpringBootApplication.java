package com.ap.testsonarwithspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSonarWithSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSonarWithSpringBootApplication.class, args);
	}

}
